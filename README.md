### Descriptions ###
We have presented three benchmark PDDL+ domains, namely: car domain, generator-events domain and planetary lander domain. For each domain we have presented the original planning domain and two planning problem instances. We have used three state-of-the-art planner to demonstrate our framework. 

For each contrastive question presented in the paper, the corresponding domain file and the problem instance is placed in the folder named with planner used. For example, under the SMT folder QN1_domain1.ppdl and QN1_problem1.pddl represent the modified domain and problem instance for the contrastive question 1.
## Prerequisites

1. Install SMTPlan+, UPMurphi, ENHSP


## Commands to run

1. For SMTPlan+:
	./SMTPLAN <domain_file> <problem_file>

2. For ENHSP:
	java -jar enhsp-dist/enhsp.jar -o <domain_file> -f <problem_file> 

3. For UPMurphi:
	a) upmc <domain_file> <problem_file> 
	b) ./<domain_file_planner>






