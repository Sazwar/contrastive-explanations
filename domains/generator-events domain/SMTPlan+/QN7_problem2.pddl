(define (problem run-generatorplus)
    (:domain generatorplus)
    (:objects gen - generator)
    (:init
		(= (fuelLevel gen) 900)
		(= (capacity gen) 1600)

		(= (fuelInTank_tank1) 40)
		(= (fuelInTank_tank2) 40)
		(= (fuelInTank_tank3) 40)

                (not (generatorStarted gen))
		(available_tank1)

		(safe gen)
		(= (s) 0)

     )  
     (:goal (generator-ran))
)
