(define (domain generatorplus)
(:requirements :fluents :durative-actions :duration-inequalities :adl :typing :time :negative-preconditions)
(:types generator)
(:predicates (generator-ran) (available_tank1) (available_tank2) (available_tank3) (using_tank1 ?g - generator) (using_tank2 ?g - generator) (using_tank3 ?g - generator) (safe ?g - generator) (generatorStarted ?g - generator))
(:functions (fuelLevel ?g - generator) (capacity ?g - generator) (fuelInTank_tank1) (fuelInTank_tank2) (fuelInTank_tank3) (ptime_tank1) (ptime_tank2) (ptime_tank3) (dur ?g - generator) (s) )


(:action generateStart
 :parameters (?g - generator)
 :precondition (and (not (generatorStarted ?g)) (>= (fuelLevel ?g) 0) (safe ?g))
 :effect (and (generatorStarted ?g) (assign (dur ?g) 0) (increase (s) 1) )
)

(:process generateProcess
 :parameters (?g - generator)
 :precondition (and (generatorStarted ?g))
 :effect (and (decrease (fuelLevel ?g) (* #t 1)) 
	      (increase (dur ?g) (* #t 1)) )	      
)

(:event generateFail
 :parameters (?g - generator)
 :precondition (and (generatorStarted ?g) (not (= (dur ?g) 1000)) 
                    (not (>= (fuelLevel ?g) 0)) )
 :effect (and (assign (dur ?g) 1001) )
)

(:action generateEnd
 :parameters (?g - generator)
 :precondition (and (generatorStarted ?g) (>= (fuelLevel ?g) 0) (safe ?g) (= (dur ?g) 1000) )
 :effect (and (generator-ran) (not (generatorStarted ?g)) )
)

(:action refuel_tank1
 :parameters (?g - generator)
 :precondition (and (not (using_tank1 ?g)) (available_tank1) )
 :effect (and (using_tank1 ?g) (not (available_tank1)) (available_tank2) (increase (s) 1))
)

(:action refuel_tank2
 :parameters (?g - generator)
 :precondition (and (not (using_tank2 ?g)) (available_tank2))
 :effect (and (using_tank2 ?g) (not (available_tank2)) (available_tank3) (increase (s) 1))
)

(:action refuel_tank3
 :parameters (?g - generator)
 :precondition (and (not (using_tank3 ?g)) (available_tank3) (> (s) 2) )
 :effect (and (using_tank3 ?g) (not (available_tank3)) )
)

(:process refuelling_tank1
 :parameters (?g - generator)
 :precondition (and (using_tank1 ?g) )
 :effect (and (decrease (fuelInTank_tank1) (* #t (* 0.001 (* (ptime_tank1) (ptime_tank1))))) 
	      (increase (ptime_tank1) (* #t 1))
  	      (increase (fuelLevel ?g) (* #t (* 0.001 (* (ptime_tank1) (ptime_tank1))))) )	      
)

(:process refuelling_tank2
 :parameters (?g - generator)
 :precondition (and (using_tank2 ?g) )
 :effect (and (decrease (fuelInTank_tank2) (* #t (* 0.001 (* (ptime_tank2) (ptime_tank2))))) 
	      (increase (ptime_tank2) (* #t 1))
  	      (increase (fuelLevel ?g) (* #t (* 0.001 (* (ptime_tank2) (ptime_tank2))))) )	      
)

(:process refuelling_tank3
 :parameters (?g - generator)
 :precondition (and (using_tank3 ?g) )
 :effect (and (decrease (fuelInTank_tank3) (* #t (* 0.001 (* (ptime_tank3) (ptime_tank3))))) 
	      (increase (ptime_tank3) (* #t 1))
  	      (increase (fuelLevel ?g) (* #t (* 0.001 (* (ptime_tank3) (ptime_tank3))))) )	      
)
     
(:event tankEmpty_tank1
 :parameters (?g - generator)
 :precondition (and (using_tank1 ?g) (<= (fuelInTank_tank1) 0))
 :effect (and (not (using_tank1 ?g)) )
)

(:event tankEmpty_tank2
 :parameters (?g - generator)
 :precondition (and (using_tank2 ?g) (<= (fuelInTank_tank2) 0))
 :effect (and (not (using_tank2 ?g)) )
)

(:event tankEmpty_tank3
 :parameters (?g - generator)
 :precondition (and (using_tank3 ?g) (<= (fuelInTank_tank3) 0))
 :effect (and (not (using_tank3 ?g)) )
)

(:event generatorOverflow
 :parameters (?g - generator)
 :precondition (and (> (fuelLevel ?g) (capacity ?g)) (safe ?g))
 :effect (and (not (safe ?g)))
)

)
