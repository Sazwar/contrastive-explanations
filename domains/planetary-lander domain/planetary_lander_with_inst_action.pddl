(define (domain planetary)
(:requirements :typing :fluents :time :negative-preconditions :timed-initial-literals)
(:types equipment)
(:predicates (day) (commsOpen) (readyForObs1) (readyForObs2) (gotObs1) (gotObs2) (available ?e - equipment) (fullPrepareStarted ?e - equipment) (prepareObs1Started ?e - equipment) (prepareObs2Started ?e - equipment) (observe1Started ?e - equipment) (observe2Started ?e - equipment) )
(:functions (demand) (supply) (soc) (charge_rate) (daytime) (heater_rate) (dusk) (dawn) (fullprepare_durtime) (prepareobs1_durtime)  (prepareobs2_durtime) (observe1_durtime) (observe2_durtime) (obs1_rate) (obs2_rate) (A_rate) (B_rate) (C_rate) (D_rate) (safeLevel) (solar_const) (fullPrepareDur ?e - equipment) (prepareObs1Dur ?e - equipment) (prepareObs2Dur ?e - equipment) (observe1Dur ?e - equipment) (observe2Dur ?e - equipment) )

(:process charging
:parameters ()
:precondition (and (< (demand) (supply)) (day))
:effect (and (increase (soc) (* #t (* (* (- (supply) (demand)) (charge_rate)) (- 100 (soc))) )))
)

(:process discharging
:parameters ()
:precondition (> (demand) (supply))
:effect (decrease soc (* #t (- (demand) (supply))))
)

(:process generating
:parameters ()
:precondition (day)
:effect (and (increase (supply)
(* #t (* (* (solar_const) (daytime)) (+ (* (daytime) (- (* 4 (daytime)) 90)) 450)))) (increase (daytime) (* #t 1)))
)

(:process night_operations
:parameters ()
:precondition (not (day))
:effect (and (increase (daytime) (* #t 1))
(decrease (soc) (* #t (heater_rate))))
)

(:event nightfall
:parameters ()
:precondition (and (day) (>= (daytime) (dusk)))
:effect (and (assign (daytime) (- (dawn)))
(not (day)))
)

(:event daybreak
:parameters ()
:precondition (and (not (day)) (>= (daytime) 0))
:effect (day)
)


(:action fullPrepareStart
:parameters (?e - equipment)
:precondition (and (available ?e) (> (soc) (safelevel)) (not (fullPrepareStarted ?e)))
:effect (and (fullPrepareStarted ?e) 
           (assign (fullPrepareDur ?e) 0)
           (not (available ?e))
           (increase (demand) (A_rate)) )
)

(:process fullPrepareProcess
:parameters (?e - equipment)
:precondition (and (fullPrepareStarted ?e) )
:effect (and (increase (fullPrepareDur ?e) (* #t 1)) )
)

(:event fullPrepareFail
:parameters (?e - equipment)
:precondition (and (fullPrepareStarted ?e)
     (not (= (fullPrepareDur ?e) (fullprepare_durtime)))
     (not (> (soc) (safelevel))) )
:effect (and (assign (fullPrepareDur ?e) (+ (fullprepare_durtime) 1)) )
)

(:action fullPrepareEnd
:parameters (?e - equipment)
:precondition (and (fullPrepareStarted ?e)
          (= (fullPrepareDur ?e) (fullprepare_durtime))
          (> (soc) (safelevel)) )
:effect (and (not (fullPrepareStarted ?e))
        (available ?e)
        (decrease (demand) (A_rate))
        (readyForObs1)
        (readyForObs2) )
)


(:action prepareObs1Start
:parameters (?e - equipment)
:precondition (and (available ?e) (> (soc) (safelevel))
          (not (prepareObs1Started ?e)) )
:effect (and (not (available ?e))
        (increase (demand) (B_rate))
        (prepareObs1Started ?e)
        (assign (prepareObs1Dur ?e) 0) )
)


(:process prepareObs1Process
:parameters (?e - equipment)
:precondition (and (prepareObs1Started ?e) )
:effect (and (increase (prepareObs1Dur ?e) (* #t 1)) )
)


(:event prepareObs1Fail
:parameters (?e - equipment)
:precondition (and (prepareObs1Started ?e)
     (not (= (prepareObs1Dur ?e) (prepareobs1_durtime)))
     (not (> (soc) (safelevel))) )
:effect (and (assign (prepareObs1Dur ?e) (+ (prepareobs1_durtime) 1)) )
)


(:action prepareObs1End
:parameters (?e - equipment)
:precondition (and (PrepareObs1Started ?e)
          (= (prepareObs1Dur ?e) (prepareobs1_durtime))
          (> (soc) (safelevel)) )
:effect (and (not (PrepareObs1Started ?e))
        (available ?e)
        (decrease (demand) (B_rate))
        (readyForObs1) )
)


(:action prepareObs2Start
:parameters (?e - equipment)
:precondition (and (available ?e) (> (soc) (safelevel))
          (not (prepareObs2Started ?e)) )
:effect (and (not (available ?e))
        (increase (demand) (C_rate))
        (prepareObs2Started ?e)
        (assign (prepareObs2Dur ?e) 0) )
)


(:process prepareObs2Process
:parameters (?e - equipment)
:precondition (and (prepareObs2Started ?e) )
:effect (and (increase (prepareObs2Dur ?e) (* #t 1)) )
)


(:event prepareObs2Fail
:parameters (?e - equipment)
:precondition (and (prepareObs2Started ?e)
     (not (= (prepareObs2Dur ?e) (prepareobs2_durtime)))
     (not (> (soc) (safelevel))) )
:effect (and (assign (prepareObs2Dur ?e) (+ (prepareobs2_durtime) 1)) )
)


(:action prepareObs2End
:parameters (?e - equipment)
:precondition (and (PrepareObs2Started ?e)
          (= (prepareObs2Dur ?e) (prepareobs2_durtime))
          (> (soc) (safelevel)) )
:effect (and (not (PrepareObs2Started ?e))
        (available ?e)
        (decrease (demand) (C_rate))
        (readyForObs2) )
)


(:action observe1Start
:parameters (?e - equipment)
:precondition (and (not (observe1Started ?e))
           (available ?e)
           (readyForObs1)
           (> (soc) (safelevel))
           (not (commsOpen)) )
:effect (and (not (available ?e)) 
           (increase (demand) (obs1_rate))
           (observe1Started ?e)
           (assign (observe1Dur ?e) 0) )
)


(:process observe1Process
:parameters (?e - equipment)
:precondition (and (observe1Started ?e) )
:effect (and (increase (observe1Dur ?e) (* #t 1)) )
)


(:event observe1Fail
:parameters (?e - equipment)
:precondition (and (observe1Started ?e)
   (not (= (observe1Dur ?e) (observe1_durtime)))
     (not (> (soc) (safelevel)))
     (not (commsOpen)) )
:effect (and (assign (observe1Dur ?e) (+ (observe1_durtime) 1)) )
)


(:action observe1End
:parameters (?e - equipment)
:precondition (and (observe1Started ?e)
         (= (observe1Dur ?e) (observe1_durtime))
         (> (soc) (safelevel))
         (not (commsOpen)) )
:effect (and (not (observe1Started ?e))
        (available ?e)
        (decrease (demand) (obs1_rate))
        (not (readyForObs1))
        (gotObs1) )
)



(:action observe2Start
:parameters (?e - equipment)
:precondition (and (not (observe2Started ?e))
           (available ?e)
           (readyForObs2)
           (> (soc) (safelevel))
           (not (commsOpen)) )
:effect (and (not (available ?e)) 
           (increase (demand) (obs2_rate))
           (observe2Started ?e)
           (assign (observe2Dur ?e) 0) )
)


(:process observe2Process
:parameters (?e - equipment)
:precondition (and (observe2Started ?e) )
:effect (and (increase (observe2Dur ?e) (* #t 1)) )
)


(:event observe2Fail
:parameters (?e - equipment)
:precondition (and (observe2Started ?e)
   (not (= (observe2Dur ?e) (observe2_durtime)))
     (not (> (soc) (safelevel)))
     (not (commsOpen)) )
:effect (and (assign (observe2Dur ?e) (+ (observe2_durtime) 1)) )
)


(:action observe2End
:parameters (?e - equipment)
:precondition (and (observe2Started ?e)
         (= (observe2Dur ?e) (observe2_durtime))
         (> (soc) (safelevel))
         (not (commsOpen)) )
:effect (and (not (observe2Started ?e))
        (available ?e)
        (decrease (demand) (obs2_rate))
        (not (readyForObs2))
        (gotObs2) )
)

)


