(define (domain car)
(:requirements :typing :durative-actions :fluents :time :negative-preconditions :timed-initial-literals)
(:predicates (running) (engineBlown) (goalReached) (has_done_acc1) (has_done_acc2))
(:functions (d) (v) (a) (upLimit) (downLimit) (runningTime))

(:process moving
:parameters ()
:precondition (and (running))
:effect (and (increase (v) (* #t (a)))
             (increase (d) (* #t (v)))
	     (increase (runningTime) (* #t 1))))

(:action accelerate1
  :parameters()
  :precondition (and (running))
  :effect (and (increase (a) 1) (has_done_acc1)))

(:action accelerate2
  :parameters()
  :precondition (and (running) (has_done_acc1))
  :effect (and (increase (a) 1) (has_done_acc2)))

(:action decelerate
  :parameters()
  :precondition (and (running) (> (a) (downLimit)) (has_done_acc2))
  :effect (and (decrease (a) 1)))

(:event engineExplode
:parameters ()
:precondition (and (running) (>= (a) 1) (>= (v) 100))
:effect (and (not (running)) (engineBlown) (assign (a) 0)))

(:action stop
:parameters()
:precondition(and (= (v) 0) (>= (d) 30) (not (engineBlown)) (has_done_acc2))
:effect(goalReached)))
