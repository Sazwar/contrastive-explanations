(define (domain car)
(:requirements :typing :durative-actions :fluents :time :negative-preconditions :timed-initial-literals)
(:predicates (running) (engineBlown) (goalReached))
(:functions (d) (v) (a) (upLimit) (downLimit) (runningTime) (s))
(:process moving
:parameters ()
:precondition (and (running))
:effect (and (increase (v) (* #t (a)))
             (increase (d) (* #t (v)))
	     (increase (runningTime) (* #t 1))))
(:action accelerate
  :parameters()
  :precondition (and (running) (< (a) (upLimit)))
  :effect (and (increase (a) 1)))
(:action decelerate'
  :parameters()
  :precondition (and (running) (> (a) (downLimit)))
  :effect (and (decrease (a) 1) (increase (s) 1)))
(:event engineExplode
:parameters ()
:precondition (and (running) (>= (a) 1) (>= (v) 100))
:effect (and (not (running)) (engineBlown) (assign (a) 0)))
(:action stop
:parameters()
:precondition(and (= (v) 0) (>= (d) 30) (not (engineBlown)))
:effect(goalReached)))
