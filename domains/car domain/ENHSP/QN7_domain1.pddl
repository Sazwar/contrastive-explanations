(define (domain car)
(:requirements :typing :durative-actions :fluents :time :negative-preconditions :timed-initial-literals)

(:predicates (running) (engineBlown) (goal_reached) (st_preseq) (has_done_pre1) (has_done_pre2) (has_done_pre3) (has_done_pre4) (has_done_preseq) (st_postseq) (has_done_postseq) (has_done_post1) (has_done_post2) (has_done_post3) (has_done_post4) (has_done_post5) (has_done_post6))

(:functions (d) (v) (a) (up_limit) (down_limit) (running_time) (c))

(:process moving
:parameters ()
:precondition (and (running))
:effect (and (increase (v) (* #t (a)))
             (increase (d) (* #t (v)))
	     (increase (running_time) (* #t 1)) )
)

(:action decelerate_pre1
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (st_preseq) (= (c) -1))
  :effect (and (decrease (a) 1) (has_done_pre1))
)

(:action accelerate_pre2
  :parameters()
  :precondition (and (running) (< (a) (up_limit)) (has_done_pre1) (= (c) -1))
  :effect (and (increase (a) 1) (has_done_pre2))
)

(:action accelerate_pre3
  :parameters()
  :precondition (and (running) (< (a) (up_limit)) (has_done_pre2) (= (c) -1))
  :effect (and (increase (a) 1) (has_done_pre3))
)

(:action decelerate_pre4
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (has_done_pre3) (= (c) -1))
  :effect (and (decrease (a) 1) (has_done_pre4))
)

(:action accelerate_pre5
  :parameters()
  :precondition (and (running) (< (a) (up_limit)) (has_done_pre4) (= (c) -1))
  :effect (and (increase (a) 1) (has_done_preseq))
)

(:event engineExplode
:parameters ()
:precondition (and (running) (>= (a) 1) (>= (v) 100))
:effect (and (not (running)) (engineBlown) (assign (a) 0))
)

(:action decelerate1
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (has_done_preseq))
  :effect (and (decrease (a) 1) (assign (c) 1))
)

(:action decelerate1
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (has_done_preseq) (= (c) -1))
  :effect (and (decrease (a) 1) (assign (c) 1))
)

(:action decelerate11
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (has_done_preseq) (not (= (c) 1)) 
(not (= (c) -1)))
  :effect (and (decrease (a) 1) (assign (c) 0))
)

(:action decelerate2
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (has_done_preseq) (= (c) 1))
  :effect (and (decrease (a) 1) (assign (c) 2))
)

(:action accelerate1
  :parameters()
  :precondition (and (running) (< (a) (up_limit)) (has_done_preseq) (= (c) 2))
  :effect (and (increase (a) 1) (assign (c) 3))
)

(:action accelerate2
  :parameters()
  :precondition (and (running) (< (a) (up_limit)) (has_done_preseq) (not (= (c) 3)))
  :effect (and (increase (a) 1) (assign (c) 0))
)

(:action stop
:parameters()
:precondition(and (= (v) 0) (>= (d) 30) (not (engineBlown)) (has_done_preseq))
:effect(and (goal_reached) (assign (c) 0))
)

(:action decelerate_post1
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (has_done_preseq) (st_postseq) (= (c) 0))
  :effect (and (decrease (a) 1) (has_done_post1))
)

(:action decelerate_post2
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (has_done_post1))
  :effect (and (decrease (a) 1) (has_done_post2))
)

(:action accelerate_post3
  :parameters()
  :precondition (and (running) (< (a) (up_limit)) (has_done_post2))
  :effect (and (increase (a) 1) (has_done_post3))
)

(:action accelerate_post4
  :parameters()
  :precondition (and (running) (< (a) (up_limit)) (has_done_post3))
  :effect (and (increase (a) 1) (has_done_post4))
)

(:action decelerate_post5
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (has_done_post4))
  :effect (and (decrease (a) 1) (has_done_post5))
)

(:action decelerate_post6
  :parameters()
  :precondition (and (running) (> (a) (down_limit)) (has_done_post5))
  :effect (and (decrease (a) 1) (has_done_post6))
)

(:action stop_post7
:parameters()
:precondition(and (= (v) 0) (>= (d) 30) (not (engineBlown)) (has_done_post6))
:effect(and (goal_reached) (assign (c) 0) (has_done_postseq))
)

)
